Change Log
==========

v2019.1: 2019/02/08
-------------------

* Command line:

  - Add command `xsocs concat` to merge multiple HDF5 master files into one (MR: !54)
  - Add option `--numcores` to set number of cores to use, e.g., `xsocs gui --numcores 2` (MR: !78)
  - Add option `--no-3d` to disable OpenGL: `xsocs gui --no-3d` (MR: !84)

* Merge:

  - Add image ROI support to only save part of input images (MR: !60)
  - Read calibration and energy from spec when available (MR: !65)
  - Allow to merge inconsistent commands (MR: !56)

* Intensity view:

  - Allow to sort scans by any positioner not just eta (MR: !58)
  - Add colorbar and option to change scatter symbols and size (MR: !64)
  - Add selection of normalization (MR: !53)
  - Shift editor: Allow to display any measurement rather than intensity (MR: !97, !106)

* QSpace conversion:

  - Add QSpace spherical coordinates system (MR: !89)
  - Add image mask (MR: !59, !66)
  - Add maxipix correction (MR: !69)
  - Add multiple energies scan support (MR: !94, !98)
  - Add optional normalization (MR: !53)
  - Provide a default number of bins for QSpace histogram (MR: !73, !104)
  - Allow to override energy and calibration (MR: !50)
  - Update helper API (MR: !90)
  - Change HDF5 file management (MR: !111, !112)
  - QSpace view: Add a stack view of the QSpace as an alternative to 3D view (MR: !72)
  - QSpace view: Add a plot with the data histogram (MR: !108)

* Fit:

  - Add background subtraction of constant and 'snip' background (MR: !85, !86, !92, !103)
  - Improve QSpace projection on axes: normalize after projection (MR: !101, !102)
  - Add tests for COM (MR: !107)

* Compatibility:

  - Fix Python3 compatibility issues (Merge requests (MR): !44, !46, !51)
  - Add support of PyQt5 and drop PyQt4 support (MR: !61)
  - Deprecates Python2 support
  - Add dependency to fabio for EDF file reading (MR: !71, !77)
  - Add Windows support (MR: !74)

* Miscellaneous:

  - GUI: Usability improvements (MR: !47, !48, !49, !55, !68, !83, !95)
  - Tests: Use gitlab-ci for continuous integration on Linux (MR: !76, !110)
  - HDF5: Use gzip compression and allow to configure it (MR: !105)
  - Minor bug fixes (MR: !45, !70, !80, !91, !96, !99)
  - Clean-up, code style and project structure (MR: !62, !63, !82, !87, !88, !93)
  - Update documentation (MR: !52, !79, !109, !113)
  - Update to newer versions of dependencies (MR: !81)

  v2017.1: 2017/12/15
  -------------------
